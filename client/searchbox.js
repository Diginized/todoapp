Template.searchbox.helpers({
    'numrecords': function(){
        //return the number of records in the database to the template variable 'numrecords'
        return Tasks.find().count()
    }
});

Template.searchbox.events({
  "keyup .manualsearch": function(event) {
    //on keyup, place the contents of .manualsearch into a session variable that is being watched by the viewitems.js:items() helper
    var text = $(event.target).val().trim();
    Session.set("manualsearch", text)
  }
});
