Template.addnew.events({
    'submit form': function (event) {
	    event.preventDefault() //stop the form from refreshing the page, allow meteor to handle it from here
	    newitemname = event.target.newitemname.value //save the item name textbox value to a variable

	    //save the dropdown value to a variable, or default it to "normal"
			if (event.target.priority.value) {
				priority = event.target.priority.value 
			}else{
				priority = "Normal"
			};

			//insert the title and priority to the database, 
			//and save the returned id to a variable (for debug purposes only at this time)
	    var insertprint = Tasks.insert(
			{title: newitemname,
				priority: priority
			}
	    )

	    console.log(insertprint, newitemname, priority) //debug/verification

	    //reset the form values 
	    event.target.newitemname.value = ''
	    event.target.priority.selected = 'Normal'
	    

    }
  });