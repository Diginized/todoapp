Template.viewitems.helpers({
    'items': function () {
    	if (Session.get('manualsearch')) {
    		//when someoone types in searchbox the 'manualsearch' session var is updated by searchbox.js
    		//the value of 'manualsearch' is inserted to a regex ("i" for case insensitive) that is passed to the mongo query
    		var query = RegExp(Session.get("manualsearch"), "i")
    		item = Tasks.find({"title": query}, {sort: {priority: -1, title: 1}}) ; 
    		console.log(query) //debug/verify
    	}else{
    		//searchbox is empty, find all records, no limit at this time.
    		item = Tasks.find({}, {sort: {priority: -1, title: 1}}) 
    	};
     	return item;
    },
    'editmode': function (){
    	//returns a T/F value to the {{#IF editmode}} {{else}} statement in the template to determine whether to render the display <span> or edit <input> modes
    	//since this is 'reactive', the state of this IF statement will change anytime the session variable changes
    	// this.$('input').focus()  //do wish this would work, but haven't figured out how to target it immediately after it goes visible
    	return Session.get("editing" + this._id) 

    }, 
    'selectedHelper': function(dbpriority, formpriority) {	
    	//insert the word "selected" into the <option> tag by comparing the database 'priority' value to the manually typed priority value.
    	if (dbpriority === formpriority) {
    		return "selected"
    	};
			/*
			something to try later: 
			this would probably be even better if I had it compre directly to the <option> element's value attribute... IF dbpriority === this.value
			*/
    }
  });

Template.viewitems.events({
	'click .delete': function(event){
		//remove the record from the database, the reactive list updates itself automatically on dbchange
		//event.target.fadeOut('slow')  //doesn't actually help with anything
		Tasks.remove(this._id)
	},
	'click .title': function(event,template){
		//when someone clicks on a record title, set its 'editing' session var to TRUE
		//the reactive helper named editmode returns the current value of this session to the template, which has an {{#IF editmode}} {{else}} to display the edit mode or not
		Session.set('editing'+this._id, true)
	},
	'click .cancel': function(event,template){
		//when someone clicks on a cancel button, set its 'editing' session var to FALSE
		//this is the reverse of the function above to change out of edit mode
		Session.set('editing'+this._id, false)
	}, 
	'keyup .edittitle': function(event){
		//the below IF statement listens for an enter key on the .edittitle input box and submits a database update, then flips the session 'editing' variable back to FALSE
		if(event.which === 13){ // 13 is enter key
			console.log(event.target.value)//debug/verify
			Tasks.update(this._id, {$set: {title: event.target.value}})
			Session.set('editing'+this._id, false)
		}
		//The below IF statement is identical to the 'click .cancel' function above, it listens for the esc key
		if(event.which === 27){ //27 is esc key
			console.log(event.target.value)//debug/verify
			Session.set('editing'+this._id, false)	
		}
	},
	'change .priority': function(event){
		//listen for a change on any of the priority dropdowns and updates the database, the reactive Tasks.find() query updates the sort automatically
		Tasks.update(this._id, {$set: {priority: event.target.value}})
		console.log(event.target.value) //debug/verify
	}
});